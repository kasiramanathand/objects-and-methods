var Mark = {
    fullName : "Mark Daniel",
    mass : "78",
    height : "1.69",
    BMI : function(){
        return this.mass / (this.height * this.height)
    }
};
var John = {
    fullName : "John Wick",
    mass : "78",
    height : "1.69",
    BMI : function(){
        return this.mass / (this.height * this.height)
    }
};

var BMIMark = Mark.BMI();
var BMIJohn = John.BMI();

if(BMIMark > BMIJohn){
    console.log(Mark.fullName + ' is greater than John\'s BMI with the score of ' + BMIMark);
}
else if(BMIMark < BMIJohn){
    console.log(John.fullName + ' is greater than John\'s BMI with the score of ' + BMIJohn);
}
else{
    console.log(John.fullName + ' & '+ Mark.fullName + ' has the same BMI.');
}